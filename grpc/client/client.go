package client

import (
	"book_magazin/b_catalog_service/config"
	"book_magazin/b_catalog_service/genproto/catalog_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	BookService() catalog_service.BooksClient
}

type grpcClients struct {
	bookService catalog_service.BooksClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	connBookService, err := grpc.Dial(
		cfg.BookServiceHost+cfg.BookGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		bookService: catalog_service.NewBooksClient(connBookService),
	}, nil
}

func (g *grpcClients) BookService() catalog_service.BooksClient {
	return g.bookService
}
