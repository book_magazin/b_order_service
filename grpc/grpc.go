package grpc

import (
	"book_magazin/b_catalog_service/config"
	"book_magazin/b_catalog_service/genproto/order_service"
	"book_magazin/b_catalog_service/grpc/client"
	"book_magazin/b_catalog_service/grpc/service"
	"book_magazin/b_catalog_service/pkg/logger"
	"book_magazin/b_catalog_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	order_service.RegisterOrdersServer(grpcServer, service.NewOrderService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
