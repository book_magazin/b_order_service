package service

import (
	"book_magazin/b_catalog_service/config"
	"book_magazin/b_catalog_service/genproto/catalog_service"
	"book_magazin/b_catalog_service/genproto/order_service"
	"book_magazin/b_catalog_service/grpc/client"
	"book_magazin/b_catalog_service/pkg/logger"
	"book_magazin/b_catalog_service/storage"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type OrderBook struct {
	*catalog_service.UnimplementedBooksServer
}

type OrderService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*order_service.UnimplementedOrdersServer
}

func NewOrderService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *OrderService {
	return &OrderService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *OrderService) Create(ctx context.Context, req *order_service.CreateOrder) (resp *order_service.Order, err error) {

	i.log.Info("---CreateOrder------>", logger.Any("req", req))

	pKey, err := i.strg.Order().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateOrder--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Order().GetById(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByIdOrder->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *OrderService) GetById(ctx context.Context, req *order_service.OrderPrimaryKey) (resp *order_service.OrderBook, err error) {

	i.log.Info("---GetOrderByID------>", logger.Any("req", req))

	res, err := i.strg.Order().GetById(ctx, req)
	if err != nil {
		i.log.Error("!!!GetOrderById->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	book, err := i.services.BookService().GetById(ctx, &catalog_service.BookPrimaryKey{Id: res.BookId})

	if err != nil {
		i.log.Error("!!!BookService->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	bookorder := &order_service.Book{
		Id:         book.Id,
		Name:       book.Name,
		AuthorId:   book.AuthorId,
		CategoryId: book.CategoryId,
		CreatedAt:  book.CreatedAt,
		UpdatedAt:  book.UpdatedAt,
	}
	
	resp = &order_service.OrderBook{
		Id:           res.Id,
		OrderedBooks: []*order_service.Book{bookorder},
		Description:  res.Description,
		CreatedAt:    res.CreatedAt,
		UpdatedAt:    res.UpdatedAt,
	}
	return
}

func (i *OrderService) GetList(ctx context.Context, req *order_service.GetListOrderRequest) (resp *order_service.GetListOrderResponse, err error) {

	i.log.Info("---GetOrders------>", logger.Any("req", req))

	resp, err = i.strg.Order().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetOrders--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *OrderService) Update(ctx context.Context, req *order_service.UpdateOrder) (resp *order_service.Order, err error) {

	i.log.Info("---UpdateOrder------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Order().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateOrder--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Order().GetById(ctx, &order_service.OrderPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetOrder->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *OrderService) Delete(ctx context.Context, req *order_service.OrderPrimaryKey) (resp *order_service.EmptyOrder, err error) {

	i.log.Info("---DeleteBook------>", logger.Any("req", req))

	resp, err = i.strg.Order().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteBook->Book->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	resp = &order_service.EmptyOrder{
		Info: "Order Delete",
	}

	return resp, nil
}
