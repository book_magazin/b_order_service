package storage

import (
	"book_magazin/b_catalog_service/genproto/order_service"
	"context"
)

type StorageI interface {
	CloseDB()
	Order() OrderRepoI
}


type OrderRepoI interface {
	Create(ctx context.Context, req *order_service.CreateOrder) (resp *order_service.OrderPrimaryKey, err error)
	GetAll(ctx context.Context, req *order_service.GetListOrderRequest) (resp *order_service.GetListOrderResponse, err error)
	Update(ctx context.Context, req *order_service.UpdateOrder) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *order_service.OrderPrimaryKey) (*order_service.EmptyOrder, error)
	GetById(ctx context.Context, req *order_service.OrderPrimaryKey) (resp *order_service.Order, err error)
}

