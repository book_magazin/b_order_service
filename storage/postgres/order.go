package postgres

import (
	// "book_magazin/b_catalog_service/genproto/catalog_service"
	"book_magazin/b_catalog_service/genproto/order_service"
	"book_magazin/b_catalog_service/pkg/helper"
	"book_magazin/b_catalog_service/storage"
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type OrderRepo struct {
	db *pgxpool.Pool
}

func NewOrderRepo(db *pgxpool.Pool) storage.OrderRepoI {
	return &OrderRepo{
		db: db,
	}
}

func (c *OrderRepo) Create(ctx context.Context, req *order_service.CreateOrder) (resp *order_service.OrderPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "order" (
				id,
				book_id,
				description,
				updated_at
			) VALUES ($1, $2, $3, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.BookId,
		req.Description,
	)

	if err != nil {
		return nil, err
	}

	return &order_service.OrderPrimaryKey{Id: id.String()}, nil
}

func (c *OrderRepo) GetById(ctx context.Context, req *order_service.OrderPrimaryKey) (resp *order_service.Order, err error) {

	query := `
		SELECT
			id,
			book_id,
			description,
			created_at,
			updated_at
		FROM "order"
		WHERE  deleted_at is null and id = $1
	`
	var (
		id          sql.NullString
		book_id     sql.NullString
		description sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&book_id,
		&description,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &order_service.Order{
		Id:          id.String,
		BookId:      book_id.String,
		Description: description.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *OrderRepo) GetAll(ctx context.Context, req *order_service.GetListOrderRequest) (resp *order_service.GetListOrderResponse, err error) {

	resp = &order_service.GetListOrderResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE  deleted_at is null and TRUE "
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			book_id,
			description,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "order"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			book_id     sql.NullString
			description sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&book_id,
			&description,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}
		// book := &catalog_service.BookPrimaryKey{
		// 	Id: book_id.String,
		// }

		resp.Orders = append(resp.Orders, &order_service.Order{
			Id:          id.String,
			BookId:      book_id.String,
			Description: description.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *OrderRepo) Update(ctx context.Context, req *order_service.UpdateOrder) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "order"
			SET
				book_id = :book_id,
				description = :description,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":          req.GetId(),
		"book_id":     req.GetBookId(),
		"description": req.GetDescription(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *OrderRepo) Delete(ctx context.Context, req *order_service.OrderPrimaryKey) (resp *order_service.EmptyOrder, err error) {

	query := `UPDATE "order"
			SET 
			deleted_at = now()
		 WHERE id = $1`

	_, err = c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return resp, nil
}
